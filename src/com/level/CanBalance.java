package com.level;

class CanBalance {
    public static void main(String[] args) {
        int[] nums = {1,1};
        System.out.println(getCanBalance(nums));
    }
    private static boolean getCanBalance(int[] nums){
        int sumFirst = 0;
        int sumSecond = 0;
        int middle = 0;
        if (nums.length%2==0){
            middle = (int) nums.length/2;
        }else {
            middle = (int)(Math.ceil(nums.length/2+1));
        }
        for (int i=0;i<middle;i++){
            sumFirst+=nums[i];
        }
        for (int i=middle;i<nums.length;i++){
            sumSecond+=nums[i];
        }
        return sumFirst==sumSecond;
    }
}
