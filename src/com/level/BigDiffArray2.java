package com.level;

class BigDiffArray2 {
    public static void main(String[] args) {
        int[] arr1 = {1,16,12,13,14};
        System.out.println(Integer.toString(getBigDiff(arr1)));
    }

    private static int getBigDiff(int[] nums){
        int min = nums[0];
        int max = nums[0];
        for (int num:nums){
            if (num<min){
                min = num;
            }
            if (num>max){
                max = num;
            }
        }
        return max-min;
    }
}
