package com.level;

class CountCodeString2 {
    public static void main(String[] args) {
        System.out.println(Integer.toString(getCountCode("codecodecde")));
    }
    private static int getCountCode(String str){
        int fromIndex = 0;
        int countCode = 0;
        while (str.indexOf("code",fromIndex)!=(-1)){
            fromIndex = str.indexOf("code",fromIndex)+4;
            countCode ++;
        }
        return countCode;
    }
}
