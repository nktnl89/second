package com.level;

import java.util.Random;

class SquareUp {
    public static void main(String[] args) {
        int[] numArray = getSquareUp(3);
        for (int num:numArray){
            System.out.print(Integer.toString(num));
        }
    }
    private static int[] getSquareUp(int n){
        int[] resultArray = new int[n*n];
        for (int i=0;i<resultArray.length;i++){
            resultArray[i] = (int) (Math.random()*n+1);
        }
        return resultArray;
    }
}
