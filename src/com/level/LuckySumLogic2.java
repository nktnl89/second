package com.level;

class LuckySumLogic2 {
    public static void main(String[] args) {
        System.out.println(Integer.toString(getLuckySum(1,13,13)));
    }
    private static int getLuckySum(int a, int b, int c){
        int sum = 0;
        int[] nums = {a,b,c};
        for (int num:nums){
            if (num==13){
                break;
            }
            sum += num;
        }
        return sum;
    }
}
